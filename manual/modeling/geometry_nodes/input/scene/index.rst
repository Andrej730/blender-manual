
##########################
  Input Scene Data Nodes
##########################

.. toctree::
   :maxdepth: 1

   collection_info.rst
   image_info.rst
   is_viewport.rst
   object_info.rst
   scene_time.rst
   self_object.rst
